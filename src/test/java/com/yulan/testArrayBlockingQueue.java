package com.yulan;

import com.yulan.threadPool.thread.FixArrayBlockingQueue;
import org.junit.Test;

import java.util.concurrent.LinkedBlockingQueue;

public class testArrayBlockingQueue {

    @Test
    public void testExtend() throws InterruptedException {
        FixArrayBlockingQueue<Integer> fixArrayBlockingQueue = new FixArrayBlockingQueue<>(10000);
        for(int i = 0;i<30000;i++){
            fixArrayBlockingQueue.add(i);

            //测试临界点能否快速扩容
            if(i == 9999)
                fixArrayBlockingQueue.setCapacity(20000);

            if(i == 10000){
                System.out.println(fixArrayBlockingQueue.items.length);
            }

            //测试临界点能否快速扩容
            if(i == 19999)
                fixArrayBlockingQueue.setCapacity(30000);

            if(i == 20000) {
                System.out.println(fixArrayBlockingQueue.items.length);
            }
        }
    }

    @Test
    public void testReduce(){
        FixArrayBlockingQueue<Integer> fixArrayBlockingQueue = new FixArrayBlockingQueue<>(10000);
        for(int i = 0; i<100; i++){
            fixArrayBlockingQueue.add(i);

            // please pick one of these to test

            //测试临界点
            if(i == 50)
                fixArrayBlockingQueue.setCapacity(49); // throw IllegalArgumentException

            if(i == 50)
                fixArrayBlockingQueue.setCapacity(100); // not throw IllegalArgumentException

        }
    }
}
