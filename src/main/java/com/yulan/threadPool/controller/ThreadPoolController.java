package com.yulan.threadPool.controller;


import com.yulan.threadPool.entity.Vo.ThreadPoolVo;
import com.yulan.threadPool.response.ServiceResponse;
import com.yulan.threadPool.service.ThreadPoolService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@Slf4j
@RequestMapping("/threadPool")
public class ThreadPoolController {

    AtomicInteger atomicInteger = new AtomicInteger(0);
    @Resource
    ThreadPoolService threadPoolService;

    @PostMapping("/test")
    public ServiceResponse providerThread(){
        return threadPoolService.testThreadPool(atomicInteger.getAndIncrement());
    }

    /**
     * 测试修改核心线程数
     */
    @PostMapping("/modifyCore/{coreSize}")
    public ServiceResponse modifyCoreSize(@PathVariable("coreSize") int coreSize){
        return threadPoolService.modifyCoreSize(coreSize);
    }

    /**
     * 测试修改最大程数
     */
    @PostMapping("/modifyMax/{MaximumPoolSize}")
    public ServiceResponse modifyMaximumPoolSize(@PathVariable("MaximumPoolSize") int MaximumPoolSize){
        return threadPoolService.modifyMaximumPoolSize(MaximumPoolSize);
    }

    /**
     * 测试修改KeepAliveTime
     */
    @PostMapping("/modifyKeepAlive/{KeepAliveTime}")
    public ServiceResponse modifyKeepAliveTime(@PathVariable("KeepAliveTime") int KeepAliveTime){
        return threadPoolService.modifyKeepAliveTime(KeepAliveTime);
    }

    /**
     * 测试修改阻塞队列容量
     */
    @PostMapping("/modifyCapacit/{capacity}")
    public ServiceResponse modifyCapacity(@PathVariable("capacity") int capacity){
        return threadPoolService.modifyCapacity(capacity);
    }


    /**
     *
     * 测试修改多项参数
     */
    @PostMapping("/modifyAll")
    public ServiceResponse modifyAll(@RequestBody ThreadPoolVo threadPoolVo){
        return threadPoolService.modifyAll(threadPoolVo);
    }




    /**
     * 获取全部参数
     */
    @GetMapping("/getAll")
    public ServiceResponse<ThreadPoolVo> getThreadPoolParams(){
        return threadPoolService.getAll();
    }


}























