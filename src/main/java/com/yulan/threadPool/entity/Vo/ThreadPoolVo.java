package com.yulan.threadPool.entity.Vo;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ThreadPoolVo {

    /**
     * 核心线程数（工作线程）
     */
    private Integer corePoolSize;

    /**
     * 最大线程数
     */
    private Integer MaximumPoolSize;

    /**
     * 多余空闲线程存活时间
     */
    private Long KeepAliveTime;

    /**
     * 队列容量
     */
    private Integer capacity;



}
