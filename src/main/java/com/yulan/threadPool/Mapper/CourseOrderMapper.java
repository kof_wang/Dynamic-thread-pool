package com.yulan.threadPool.Mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yulan.threadPool.entity.Mo.CourseOrder;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CourseOrderMapper extends BaseMapper<CourseOrder> {

    @Select("SELECT t2.`uid`,t2.`cid` FROM teacher_course t1 , student_course t2  " +
            "WHERE t1.`cid` = t2.`cid` AND t1.`cid` = #{cid} AND t1.`tid` = #{tid}")
    List<CourseOrder> getTeacherAndCourseAndStudent(int tid, int cid);
}
