package com.yulan.threadPool.service;


import com.yulan.threadPool.entity.Mo.CourseOrder;
import com.yulan.threadPool.response.ServiceResponse;
import com.yulan.threadPool.thread.DynamicThreadPool;
import com.yulan.threadPool.thread.WorkThread;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Slf4j
public class PressureTestService {


    @Resource
    CourseOrderService courseOrderService;
    /**
     * 使用线程池执行线程任务
     * @param thread 需要被执行的线程
     * @return
     */
    public ServiceResponse processMask(Thread thread){
        try{
            //获取线程池单例
            DynamicThreadPool.getDynamicThreadPool().getThreadPool().execute(thread);
            log.info("线程： {} 执行成功!",thread.getName());
            return ServiceResponse.createSuccess();
        }catch (Exception e){
            e.printStackTrace();
            return ServiceResponse.createError();
        }
    }

    @Transactional
    public ServiceResponse DoFastKill(int uid,int cid){


        CourseOrder courseOrder = CourseOrder.builder()
                .uid(uid)
                .cid(cid)
                .build();

        boolean success1 = courseOrderService.save(courseOrder);

        if(!success1) return ServiceResponse.createError();

        log.info("插入 {} 成功！线程：{}，为您服务", courseOrder.toString(),Thread.currentThread().getName());
        return  ServiceResponse.createSuccess();
    }
}
