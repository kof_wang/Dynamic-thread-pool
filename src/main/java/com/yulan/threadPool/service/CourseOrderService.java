package com.yulan.threadPool.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yulan.threadPool.entity.Mo.CourseOrder;

import java.util.List;

public interface CourseOrderService extends IService<CourseOrder> {


    List<CourseOrder> getTeacherAndCourseAndStudent(int tid, int cid);
}
