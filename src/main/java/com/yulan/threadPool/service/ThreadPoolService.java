package com.yulan.threadPool.service;


import com.yulan.threadPool.entity.Vo.ThreadPoolVo;
import com.yulan.threadPool.response.ServiceResponse;
import com.yulan.threadPool.thread.DynamicThreadPool;
import com.yulan.threadPool.thread.WorkThread;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


@Service
@Slf4j
public class ThreadPoolService {

    public ServiceResponse testThreadPool(Integer i){
        try{
            log.info("原子i：{}",i+"");
            Runnable workThread = new WorkThread("" + i);
            DynamicThreadPool.getDynamicThreadPool().getThreadPool().execute(workThread);
            return ServiceResponse.createBySuccessMsgData("运行成功",null);
        }catch (Exception e){
            e.printStackTrace();
            return ServiceResponse.createByErrorMsgData("运行失败",null);
        }
    }

    public ServiceResponse modifyCoreSize(int coreSize){
        log.info("coreSize：{}",coreSize+"");
        return DynamicThreadPool.getDynamicThreadPool().setCorePoolSize(coreSize);
    }

    public ServiceResponse modifyMaximumPoolSize(int MaximumPoolSize){
        log.info("MaximumPoolSize：{}",MaximumPoolSize+"");
        return DynamicThreadPool.getDynamicThreadPool().setMaximumPoolSize(MaximumPoolSize);
    }


    public ServiceResponse modifyKeepAliveTime(int KeepAliveTime){
        log.info("KeepAliveTime：{}",KeepAliveTime+"");
        return DynamicThreadPool.getDynamicThreadPool().setKeepAliveTime(KeepAliveTime, TimeUnit.SECONDS);
    }

    public ServiceResponse modifyCapacity(int capacity){
        log.info("capacity: {}",capacity+"");
        return DynamicThreadPool.getDynamicThreadPool().setCapacity(capacity);
    }

    public ServiceResponse modifyAll(ThreadPoolVo threadPoolVo){
        int capacity, corePoolSize, MaximumPoolSize;
        long KeepAliveTime;

        try{
            /**
             * 更新容量
             */
            if(threadPoolVo.getCapacity()!=null){
                capacity = threadPoolVo.getCapacity();
                DynamicThreadPool.getDynamicThreadPool().setCapacity(capacity);
                log.info("修改容量成功，新的容量大小: {}",DynamicThreadPool.getDynamicThreadPool().getQueueCapacity());
            }

            /**
             * 更新核心线程数
             */
            if(threadPoolVo.getCorePoolSize()!=null){
                corePoolSize = threadPoolVo.getCorePoolSize();
                DynamicThreadPool.getDynamicThreadPool().setCorePoolSize(corePoolSize);
                log.info("修改心线程数成功，新的心线程数大小: {}",
                        DynamicThreadPool.getDynamicThreadPool().getThreadPool().getCorePoolSize());
            }
            /**
             * 更新最大线程数
             */
            if(threadPoolVo.getMaximumPoolSize() !=null){
                MaximumPoolSize = threadPoolVo.getMaximumPoolSize();
                DynamicThreadPool.getDynamicThreadPool().setMaximumPoolSize(MaximumPoolSize);
                log.info("修改最大线程数成功，新的最大线程数大小: {}",
                        DynamicThreadPool.getDynamicThreadPool().getThreadPool().getMaximumPoolSize());
            }

            /**
             * 更新空闲存活时间
             */
            if(threadPoolVo.getKeepAliveTime()!=null){
                KeepAliveTime = threadPoolVo.getKeepAliveTime();
                DynamicThreadPool.getDynamicThreadPool().setKeepAliveTime(KeepAliveTime,TimeUnit.SECONDS);
                log.info("修改空闲存活时间成功，新的空闲存活时间大小: {}",
                        DynamicThreadPool.getDynamicThreadPool().getThreadPool().getKeepAliveTime(TimeUnit.SECONDS));
            }

            return ServiceResponse.createSuccess();
        }catch (Exception e){
            log.info("修改失败");
            return ServiceResponse.createError();
        }
    }

    public ServiceResponse<ThreadPoolVo> getAll(){
        //获取线程池单例
        ThreadPoolExecutor threadPoolExecutor = DynamicThreadPool.getDynamicThreadPool().getThreadPool();

        try{
            //动态设置线程池参数
            ThreadPoolVo threadPoolVo = ThreadPoolVo.builder()
                    .corePoolSize(threadPoolExecutor.getCorePoolSize())
                    .MaximumPoolSize(threadPoolExecutor.getMaximumPoolSize())
                    .capacity(DynamicThreadPool.getDynamicThreadPool().getQueueCapacity())
                    .KeepAliveTime(threadPoolExecutor.getKeepAliveTime(TimeUnit.SECONDS))
                    .build();

            return ServiceResponse.createSuccessByData(threadPoolVo);
        }catch (Exception e){
            e.printStackTrace();
            return ServiceResponse.createError();
        }
    }


}
